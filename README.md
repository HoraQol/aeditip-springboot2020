# Taller de Introducción a Spring Boot 2020

## Introducción

Estimado(a) lector(a).<br></br>
De parte del Grupo AEDITIP, te damos la bienvenida al Repositorio del Taller de Introducción a Spring Boot, desarrollado en la PUCP del 10 al 14 de marzo de 2020. Este
repositorio tiene como objetivo almacenar el código fuente desarrollado y/o mostrado en las sesiones para su futura consulta por el público en general.

## Contenido
Para mejor entendimiento del contenido. se ha decidido mantener en una carpeta diferente el contenido de cada sesión, teniendo la siguiente distribución

| Sesión | Carpeta |
| ------ | ------ |
| Presentación de Spring Boot (2020-03-10) | sesion1 |
| Patrones de diseño y temas adicionales en Spring Boot (2020-03-12) | sesion2 | 
| Documentación de servicios con Postman y despliegue continuo con Gitlab CI/CD (2020-03-14) | sesion3 |

## Contacto
La opinión y oportunidades de retroalimentación de los asistentes a nuestros talleres es muy importante. Por ello, si tienes alguna consulta, duda o sugerencia, por favor,
comunícate a los siguientes medios de comunicación.
### Correo electrónico


| Nombre | Rol | Correo |
| ------ | ------ | -----|
| [Jorge Fatama Vera](https://www.linkedin.com/in/jfatamav/) | Co-ponente del Taller y co-autor del Material de Ayuda | jfatamav@e-quipu.pe |
| [Carlos Carillo Agoyo](https://www.linkedin.com/in/carlos-carrillo-a3b7a0129/) | Co-ponente del Taller | carlosg.carrillo@pucp.edu.pe |
| [Lis Paredes Pardo](https://www.linkedin.com/in/lis-paredes-pardo-ba6b75145/) | Coordinadora General de Logística del Taller | lis.paredesp@pucp.pe |
| [André Corrales Estrada](https://www.linkedin.com/in/r%C3%B3bert-andr%C3%A9-corrales-estrada-4a1341120/) | Co-autor del Material de Ayuda | a20143250@pucp.pe |

### Páginas
[Facebook del Grupo AEDITIP](https://www.facebook.com/aeditip) <br></br>
[LinkedIn del Grupo AEDITIP](https://www.linkedin.com/company/aeditip)

## Agradecimientos
Agradecemos a E-quipu por ayudarnos a reservar el aula para el taller. También agradecemos a los miembros y colaboradores del Grupo que hicieron posible la organización del
Taller, así como la disponibilidad del material: Jorge Fatama Vera, Carlos Carillo Agoyo, Lis Paredes Pardo y André Corrales Estrada.<br></br>
Finalmente, agradecemos a cada uno de los asistentes al taller, por haberse tomado un tiempo para atender la clase y brindarnos la retroalimentación necesaria para poder
mejorar, y a las personas que nos han ayudado con la difusión del evento, cuya colaboración desinteresada ha logrado que más personas puedan beneficiarse de los conocimientos
impartidos aquí.

**Atte.<br></br>
Directorio de AEDITIP**
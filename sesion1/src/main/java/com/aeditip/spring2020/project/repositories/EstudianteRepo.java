package com.aeditip.spring2020.project.repositories;

import java.util.List;

import org.springframework.data.jdbc.repository.query.Query;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aeditip.spring2020.project.entities.Estudiante;

@Repository
public interface EstudianteRepo extends JpaRepository<Estudiante, Integer>{
	/*@Query("SELECT * FROM estudiante WHERE cicloIngreso = ?1")
	public List<Estudiante> obtenerEstudiantesEnCiclo(String ciclo);*/
}
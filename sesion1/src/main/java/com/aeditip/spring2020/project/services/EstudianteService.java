package com.aeditip.spring2020.project.services;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.aeditip.spring2020.project.entities.Estudiante;
import com.aeditip.spring2020.project.repositories.EstudianteRepo;

@Service
public class EstudianteService {
	@Autowired
	private EstudianteRepo repo;
	
	// SELECT * FROM estudiante
	public List<Estudiante> getAll(){
		return repo.findAll();
	}
	
	// SELECT * FROM estudiante WHERE codigo = {id}
	public Estudiante getById(@PathVariable int id) {
		/* Versión 1*/
		//if(repo.findById(id).isPresent())
		//	return repo.findById(id).get();
		//return null;
		/* Versión 2*/
		return (repo.findById(id).isPresent() ? repo.findById(id).get() : null);
	}
	
	// INSERT INTO estudiante VALUES (...)
	public Estudiante add(@Valid @RequestBody Estudiante estudiante) {
		return repo.save(estudiante);
	}
	
	// UPDATE estudiante SET = (...) WHERE codigo = {id}
	public Estudiante update(@RequestBody Estudiante estudiante,
			@PathVariable int id) {
		if(repo.findById(id).isPresent()) {
			// Tengo al estudiante con código {id}
			Estudiante x = repo.findById(id).get();
			x.setEscala(estudiante.getEscala());
			x.setEscala(estudiante.getEstado());
			return repo.save(estudiante);
		}
		return null;
	}
	
	// DELETE FROM estudiante WHERE codigo = {id}
	public boolean delete(@PathVariable int id) {
		if(repo.findById(id).isPresent()) {
			// Esto sirve para JpaRepository y CrudRepository
			// Estudiante x = repo.findById(id).get(); 
			// repo.delete(x);
			// Esto sirve sólo para JpaRepository
			repo.deleteById(id);
			return true;
		}
		return false;
	}
	
	// Solicitudes personalizadas
	// SELECT * FROM estudiante WHERE cicloIngreso = "2020-1"
	/*public List<Estudiante> getByCicle(@PathVariable String ciclo){
		return repo.obtenerEstudiantesEnCiclo(ciclo);
	}*/
}
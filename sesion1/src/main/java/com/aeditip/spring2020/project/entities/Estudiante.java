package com.aeditip.spring2020.project.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data

@Entity
@Table(name="estudiante")
@NoArgsConstructor
@AllArgsConstructor
public class Estudiante {
	@Id
	// Id autoincremental
	// @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "codigo", columnDefinition = "INT(8)")
	private int codigo;
	@NotEmpty
	@Column(name = "apellidoPaterno", columnDefinition = "VARCHAR(50)")
	private String apellidoPaterno;
	@Column(name = "apellidoMaterno", columnDefinition = "VARCHAR(50)")
	private String apellidoMaterno;
	@NotEmpty
	@Column(name = "nombres", columnDefinition = "VARCHAR(50)")
	private String nombres;
	@NotEmpty
	@Column(name = "ciclo", columnDefinition = "VARCHAR(7)")
	private String cicloIngreso;
	@NotNull
	@Column(name = "escala", columnDefinition = "INT(1)")
	private int escala;
	@NotNull
	@Column(name = "estado", columnDefinition = "INT(1)")
	private int estado;
	
	/*public Estudiante() {}
	
	public Estudiante(int codigo, String apellidoPaterno, String apellidoMaterno,
			String nombres, String ciclo, int escala, int estado) {
		this.codigo = codigo;
		this.apellidoPaterno = apellidoPaterno;
		this.apellidoMaterno = apellidoMaterno;
		this.nombres = nombres;
		this.cicloIngreso = ciclo;
		this.escala = escala;
		this.estado = estado;
	}*/
	
	/*public int getEscala() { return this.escala;}
	public void setEscala(int escala) {this.escala = escala;}
	
	public int getEstado() { return this.estado;}
	public void setEstado(int estado) {this.estado = estado;}*/
	
	/* Getters y setters adicionales */
}
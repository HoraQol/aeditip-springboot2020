package com.aeditip.spring2020.project.errors.nfex;

public class EstudianteNFEx extends RuntimeException {
	private static final long serialVersionUID = 1001;
	public EstudianteNFEx(int id) {
		super("No se ha encontrado al estudiante con código " + id);
	}
}
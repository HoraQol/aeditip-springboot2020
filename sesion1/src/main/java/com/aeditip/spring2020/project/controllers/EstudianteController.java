package com.aeditip.spring2020.project.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.aeditip.spring2020.project.entities.Estudiante;
import com.aeditip.spring2020.project.errors.nfex.EstudianteNFEx;
import com.aeditip.spring2020.project.services.EstudianteService;

@RestController
@RequestMapping("api")
@CrossOrigin(origins="*", methods={RequestMethod.GET, 
		RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE})
public class EstudianteController {
	@Autowired
	private EstudianteService service;
	
	// SELECT * FROM estudiante
	@RequestMapping(value = "estudiantes", method = RequestMethod.GET)
	List<Estudiante> allEstudiantes(){
		return service.getAll();
	}
	
	// SELECT * FROM estudiante WHERE codigo = {id}
	// Llega: http://localhost:8080/api/estudiante/6 
	// Lee: id = 6
	@RequestMapping(value = "estudiante/{id}", method = RequestMethod.GET)
	Estudiante estudianteById(@PathVariable int id) {
		Estudiante x = service.getById(id);
		if(x != null)
			return x;
		throw new EstudianteNFEx(id);	
	}
	
	// INSERT INTO estudiante VALUES (...)
	// Llega: http://localhost:8080/api/estudiante (POST, Objecto)
	@RequestMapping(value = "estudiante", method = RequestMethod.POST)
	Estudiante addEstudiante(@Valid @RequestBody Estudiante estudiante) {
		return service.add(estudiante);
	}
	
	// UPDATE estudiante SET = (...) WHERE codigo = {id}
	Estudiante updateEstudiante(@RequestBody Estudiante estudiante,
			@PathVariable int id) {
		Estudiante x = service.update(estudiante, id);
		if(x != null)
			return x;
		throw new EstudianteNFEx(id);
	}
	
	// DELETE FROM estudiante WHERE codigo = {id}
	void deleteEstudiante(@PathVariable int id) {
		if(!service.delete(id))
			throw new EstudianteNFEx(id);
	}
	
	// Solicitudes personalizadas
	// SELECT * FROM estudiante WHERE cicloIngreso = "2020-1"
	/*List<Estudiante> estudiantesXCiclo(@PathVariable String ciclo){
		return service.getByCicle(ciclo);
	}*/
}